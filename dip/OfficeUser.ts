import {Database} from "../single/responsibility/cohension/ex3/Database";
import {Post} from "../openclosed/ex0/Post";

export class OfficeUser {

    public publishNewPost() {
        let db: Database = new Database();
        let postMessage: String = "example message";
        let post: Post = new Post();
        post.CreatePost(db, postMessage);
    }
}
