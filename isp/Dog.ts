import {Animal} from "./Animal";

export class Dog implements Animal {
    
    public walk() {
        console.log("dog can walk");
    }
    
    public fly() {
        throw new Error();
    }
}
